
# Technologie

* [Histoire de l'informatique (première partie)](https://www.youtube.com/watch?v=dJdiSN9q5QE)
* [Histoire de l'info (seconde et dernière partie)](https://www.youtube.com/watch?v=NNxAKALRePo)
* [Une brève histoire de l'informatique, de 1945 à nos jours](https://www.youtube.com/watch?v=dcN9QXxmRqk)
* [Épisode 1/6 : Transistor : la révolution à sauts de puce](https://www.radiofrance.fr/franceculture/podcasts/entendez-vous-l-eco/transistor-la-revolution-a-saut-de-puce-9759644)
* [Timeline of Computer History](https://www.computerhistory.org/timeline/1933/)
* [Chronologie du réseau Internet](http://www.tiki-toki.com/timeline/embed/137139/6372410394/#vars!date=1954-07-27_20:20:56!)
* [Une histoire d'Internet (*Laurent Chemla*)](http://www.chemla.org/textes/hinternet.html)
* [Déclaration d’indépendance du Cyberespace (*John P. Barlow*)](http://editions-hache.com/essais/barlow/barlow2.html)
* [Une nouvelle déclaration d’indépendance du cyberespace (*Libération*)](http://www.liberation.fr/amphtml/debats/2018/02/09/une-nouvelle-declaration-d-independance-du-cyberespace_1628377)
* [<i class="fas fa-podcast"></i> Une histoire de… l'Internet (*Julien Le Bot*)](https://www.radiofrance.fr/franceculture/podcasts/serie-une-histoire-de-l-internet)
* [<i class="fas fa-book"></i> Aux sources de l'utopie numérique (*Fred turner*)](http://cfeditions.com/Turner/)
* [<i class="fas fa-book"></i> Les Rêves cybernétiques de Norbert Wiener(*Pierre Cassou-Noguès*)](https://www.seuil.com/ouvrage/les-reves-cybernetiques-de-norbert-wiener-pierre-cassou-nogues/9782021090284)
* [<i class="fas fa-book"></i> Internet, année zéro (*Jonathan Bourguignon*)](https://www.editionsdivergences.com/livre/internet-annee-zero)
* [<i class="fas fa-book"></i> L'utopie déchue (*Félix Tréguer*)](https://www.fayard.fr/sciences-humaines/lutopie-dechue-9782213710044)
* [<i class="fas fa-book"></i> Cyberstructure (*Stéphane Bortzmeyer*)](https://cfeditions.com/cyberstructure/)
- [L'histoire de Microsoft : Bill Gates et Paul Allen](https://www.youtube.com/watch?v=r8AsQ9TVKuA)
* [<i class="fab fa-youtube"></i> Jurassic Web, Une préhistoire des réseaux sociaux](https://www.youtube.com/watch?v=mLEbi2MAoL8)
* [<i class="fab fa-youtube"></i> Une contre histoire de l'Internet](https://www.youtube.com/watch?v=MUTABXD8f24)
* [<i class="fab fa-youtube"></i> The Net: The Unabomber, LSD & the Internet](https://www.youtube.com/watch?v=GY6fb59XFbQ)
* [<i class="fab fa-youtube"></i> Comprendre ce que représente le mythe de la « frontière électronique » (*Stéphanie Ouillon*)](https://video.passageenseine.fr/videos/watch/555339ff-ef64-4e18-af40-6bd79b78008b)
* [<i class="fab fa-youtube"></i> Halt and Catch Fire (série télévisée)](https://fr.wikipedia.org/wiki/Halt_and_Catch_Fire_(s%C3%A9rie_t%C3%A9l%C3%A9vis%C3%A9e))
* [<i class="fab fa-youtube"></i> Internet VS minitel 2.0 (*Benjamin Bayard*)](https://cfeditions.com/cyberstructure/)
* [<i class="fab fa-youtube"></i> The Machine That Changed The World](https://waxy.org/2008/06/the_machine_that_changed_the_world/)
    * [<i class="fab fa-youtube"></i> Les cinglés de l'informatique](https://www.youtube.com/watch?v=PH-Qjy4zVKQ&t=3s)
* [Les Figures de l'ombre](https://www.allocine.fr/film/fichefilm_gen_cfilm=219070.html)
* [<i class="fab fa-youtube"></i>Triumph of the nerds](https://www.pbs.org/nerds/)
* [<i class="fab fa-youtube"></i> The History of Computing (_Virginia Tech, US_) (avec des notes explicatives de 2 précédentes séries TV)](https://ei.cs.vt.edu/%7Ehistory/index.html)
* [<i class="fab fa-youtube"></i> AT&T Archives: The UNIX Operating System](https://www.youtube.com/watch?v=tc4ROCJYbm0)
* [<i class="fab fa-youtube"></i> HP Origins - Hewlett Packard Documentary](https://www.youtube.com/watch?v=Iqv6DhtLay4)
* [<i class="fab fa-youtube"></i> L'histoire de HP 1939](https://www.youtube.com/watch?v=0sb5sNt_Ifg)
* [<i class="fab fa-youtube"></i> cocadmin / L'histoire du tout premier ordinateur](https://www.youtube.com/watch?v=x9t9kLNbWJ4)
* [<i class="fab fa-youtube"></i> cocadmin / Les ordi étaient dla merde avant que ce génie arrive (Morris / tsmc)](https://www.youtube.com/watch?v=z9f12rod30Q&t=5s)
* [<i class="fab fa-youtube"></i> cocadmin / Linux n'est pas Unix ! - L'histoire et l'origine des deux OS](https://www.youtube.com/watch?v=baxmRgeX7-E)
* [<i class="fab fa-youtube"></i> cocadmin / Pourquoi Orange et SFR ont peur de ce génie ? (Xavier Niel)](https://www.youtube.com/watch?v=fN7mDxhN-QM)
* [<i class="fab fa-youtube"></i> Comment ça marche ? La fabuleuse aventure du micro ordinateur, 1994 , Documentaire (Maurice Chevalet)](https://www.youtube.com/watch?v=W20d8Pj2fy8)
* [<i class="fab fa-youtube"></i> L'histoire de l'informatique](https://www.youtube.com/watch?v=r6iOj7MwzZY)
* [<i class="fab fa-youtube"></i> Micral N, le premier micro-ordinateur de l'histoire](https://www.youtube.com/watch?v=ERp5KkRdYRU)
* [<i class="fab fa-youtube"></i> La théorie de l'information de Claude Shannon - Passe-science #44](https://www.youtube.com/watch?v=kD5DHGbkYz0)

#### Intelligence artificielle

* [<i class="fab fa-youtube"></i> L'asservissement par l'Intelligence Artificielle ? (*Éric Sadin chez ThinkerView*)](https://www.youtube.com/watch?v=VzeOnBRzDik)
* [<i class="fab fa-youtube"></i> Conférence ChatGPT : Une évolution technique majeure (*Vincent Guigue*)](https://webtv.uca.fr/video/2c0d336a-eb51-4e22-9415-0da2ef0eb3a6)
* [<i class="fab fa-youtube"></i> De quoi ChatGPT est-il VRAIMENT capable ? (*Monsieur Phi*)](https://www.youtube.com/watch?v=R2fjRbc9Sa0)
* [<i class="fab fa-youtube"></i> cocadmin / Comment l'intelligence artificielle est morte 2 fois ?](https://www.youtube.com/watch?v=RKtR-lSOq2g)
* [<i class="fab fa-youtube"></i> Les machines et les hommes : l'intelligence artificielle (1972) | Rembob'INA](https://www.youtube.com/watch?v=bTv0MfRAwn8)
* [<i class="fab fa-youtube"></i> Albert Ducrocq, le génie français de la cybernétique [1958]](https://www.youtube.com/watch?v=u2NxSHdtJKg)
* [<i class="fab fa-book"></i> Contre-atlas de l’intelligence artificielle (*Kate Crawford*)](https://www.zulma.fr/livre/contre-atlas-de-lintelligence-artificielle/)

## Bande son

- Voyage dans les internets
    - [1/5 60/70's : l'écho psychédélique du bout des tuyaux](https://www.radiofrance.fr/franceculture/podcasts/culture-musique-ete/60-70-s-l-echo-psychedelique-du-bout-des-tuyaux-1705776)
    - [2/5 70/80's : cowboys nomades, astragale et feux de camps électriques](https://www.radiofrance.fr/franceculture/podcasts/culture-musique-ete/70-80-s-cowboys-nomades-astragale-et-feux-de-camps-electriques-7094566)
    - [3/5 Les années 1990 : Un Web et des bulles](https://www.radiofrance.fr/franceculture/podcasts/culture-musique-ete/les-annees-1990-un-web-et-des-bulles-6566075)
    - [4/5 : Les années 2000 : sous les pop-up, l'indépendance](https://www.radiofrance.fr/franceculture/podcasts/culture-musique-ete/les-annees-2000-sous-les-pop-up-l-independance-3189017)
    - [5/5 : Un Internet à la dérive. Des internets en résistance](https://www.radiofrance.fr/franceculture/podcasts/culture-musique-ete/un-internet-a-la-derive-des-internets-en-resistance-6466951)


## Podcasts

- [Le code a changé](https://www.radiofrance.fr/franceinter/podcasts/le-code-a-change)
- [CyberPouvoirs](https://www.radiofrance.fr/franceinter/podcasts/cyberpouvoirs)
* La vie d'Alan Turing
    - [Épisode 1/4 : Enigma, la guerre du code](https://www.radiofrance.fr/franceculture/podcasts/grande-traversee-l-enigmatique-alan-turing/enigma-la-guerre-du-code-2085217)
    - [Épisode 2/4 : Des marguerites à l’ordinateur](https://www.radiofrance.fr/franceculture/podcasts/grande-traversee-l-enigmatique-alan-turing/des-marguerites-a-l-ordinateur-1510370)
    - [Épisode 3/4 : Le bug](https://www.radiofrance.fr/franceculture/podcasts/grande-traversee-l-enigmatique-alan-turing/le-bug-5843912)
    - [Épisode 4/4 : Les mythologies d’Alan Turing](https://www.radiofrance.fr/franceculture/podcasts/grande-traversee-l-enigmatique-alan-turing/les-mythologies-d-alan-turing-5870730)
* [La double vie de Norbert Wiener : le père de la cybernétique !](https://www.radiofrance.fr/franceculture/podcasts/la-marche-des-sciences/la-double-vie-de-norbert-wiener-le-pere-de-la-cybernetique-1832904)


